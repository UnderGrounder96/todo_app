# todo_app

## Why?

I have decided to create my own TODO repo, using preactJS.

## Where?

https://undergrounder96.gitlab.io/todo_app/

## How?

Docker:

```bash
# (with) docker-compose
docker-compose up
```

k8s:

```bash
# Build docker image
docker build --tag back-end_image back-end/
docker build --tag front-end_image front-end/
docker push ... # push docker images to node/repository

# Deploy to k8s
kubectl apply -f mongo/k8s/
kubectl apply -f back-end/k8s/
kubectl apply -f front-end/k8s/
```
