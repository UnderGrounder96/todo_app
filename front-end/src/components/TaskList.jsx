const Title = () => {
  return <h3 class="title">task table</h3>;
};

const List = ({ axios, table, setTable, setSearchID }) => {
  const formatDate = (originalDate) => {
    const [day, month, year] = originalDate.split("-");
    const date = new Date();

    const monthNames = [
      "Jan",
      "Feb",
      "Mar",
      "Apr",
      "May",
      "Jun",
      "Jul",
      "Aug",
      "Sep",
      "Oct",
      "Nov",
      "Dec",
    ];

    const dateFormat = `${day}/${monthNames[month - 1]}`;

    if (date.getFullYear() == year) {
      return dateFormat;
    }

    return `${dateFormat}/${year}`;
  };

  return (
    <ul>
      {table.map(({ id, title, desc, date }) => (
        <li class="task" key={id}>
          <div class="task-column task-left">
            <span class="task-title">{title}</span>
            <i class="task-date">{formatDate(date)}</i>
          </div>
          <div class="task-column task-right">
            <button class="btn btn-funky" onclick={() => setSearchID(id)}>
              edit
            </button>
            <button
              class="btn alert-danger"
              onclick={() =>
                axios
                  .delete(`/${id}`)
                  .then(() => setTable(table.filter((item) => item.id !== id)))
                  .catch((error) => console.error(`error: ${error}`))}
            >
              delete
            </button>
          </div>
        </li>
      ))}
    </ul>
  );
};

export default function TaskList({ axios, table, setTable, setSearchID }) {
  return (
    <>
      <Title />
      <List
        axios={axios}
        table={table}
        setTable={setTable}
        setSearchID={setSearchID}
      />
    </>
  );
}
