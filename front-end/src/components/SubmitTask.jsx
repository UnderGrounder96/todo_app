import { useEffect, useRef } from "preact/hooks";

const Title = () => {
  return <h3 class="title">add new task</h3>;
};

const TaskForm = ({ axios, table, setTable, searchID, setSearchID }) => {
  const task = useRef(null);
  const desc = useRef(null);

  useEffect(() => {
    if (searchID) {
      const { title, desc: fDesc } = table.find(
        (element) => element.id === searchID,
      );

      task.current.value = title;
      desc.current.value = fDesc;
    }
  }, [searchID]);

  const handleSubmit = async (task, desc) => {
    const date = new Date();
    const dateFormat = `${date.getDate()}-${
      date.getMonth() + 1
    }-${date.getFullYear()}`;

    const element = {
      title: task,
      desc,
      date: dateFormat,
    };

    const alertDiv = document.querySelector(".alert");

    try {
      let response;

      if (searchID) {
        response = await axios.patch(`/${searchID}`, { data: element });

        if (response.data) {
          setTable(
            table.map((item) => {
              if (item.id === searchID) {
                return { id: searchID, ...element };
              }

              return item;
            }),
          );
          setSearchID(null);
          alertDiv.innerText = "task updated";
          alertDiv.classList.toggle("alert-success");
        } else {
          alertDiv.innerText = "task update failed";
          alertDiv.classList.toggle("alert-danger");
          console.error("error: No content received!");
        }
      } else {
        response = await axios.post("/", { data: element });

        if (response.data) {
          element.id = response.data.body;
          setTable((previousState) => [...previousState, element]);
          alertDiv.innerText = "task added";
          alertDiv.classList.toggle("alert-success");
        } else {
          alertDiv.innerText = "task added failed";
          alertDiv.classList.toggle("alert-danger");
          console.error("error: No content received!");
        }
      }
    } catch (error) {
      alertDiv.innerText = "error reaching gateway";
      alertDiv.classList.toggle("alert-danger");
      console.error(`error: ${error}`);
    } finally {
      setTimeout(() => {
        alertDiv.classList.remove("alert-danger", "alert-success");
      }, 4000);
    }
  };

  return (
    <form
      onsubmit={(e) => {
        e.preventDefault();
        handleSubmit(task.current.value, desc.current.value);
        e.target.reset();
      }}
      autocomplete="on"
    >
      <div class="form-row">
        <div class="alert" />
      </div>

      <div class="form-row">
        <label for="task">task</label>
        <input
          required
          id="task"
          ref={task}
          type="text"
          name="task"
          maxlength="30"
          placeholder="Start new preact project"
        />
      </div>

      <div class="form-row">
        <label for="desc">desc.</label>
        <textarea
          required
          id="desc"
          ref={desc}
          name="desc"
          maxlength="150"
          placeholder="Your description goes here"
        />
      </div>

      <div class="form-row">
        <button type="submit">submit</button>
      </div>
    </form>
  );
};

export default function SubmitTask({
  axios,
  table,
  setTable,
  searchID,
  setSearchID,
}) {
  return (
    <>
      <Title />
      <TaskForm
        axios={axios}
        table={table}
        setTable={setTable}
        searchID={searchID}
        setSearchID={setSearchID}
      />
    </>
  );
}
