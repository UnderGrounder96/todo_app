import { useEffect, useState } from "preact/hooks";
import axios from "axios";

import SubmitTask from "./SubmitTask.jsx";
import TaskList from "./TaskList.jsx";

// import { data } from "/src/api/data.jsx";

export default function Hero() {
  axios.defaults.baseURL =
    `http://${import.meta.env.APP_GATEWAY_HOST}:${import.meta.env.APP_GATEWAY_PORT}`;

  const [table, setTable] = useState([]);
  const [searchID, setSearchID] = useState(null);

  useEffect(() => {
    const getAllData = async () => {
      try {
        const response = await axios.get("/");

        if (response && response.data) {
          setTable(response.data.body);
        }
      } catch (error) {
        console.error(`error: ${error}`);
      }
    };

    getAllData();
  }, []);

  return (
    <main class="section-center main">
      <SubmitTask
        axios={axios}
        table={table}
        setTable={setTable}
        searchID={searchID}
        setSearchID={setSearchID}
      />
      <br />
      <TaskList
        axios={axios}
        table={table}
        setTable={setTable}
        setSearchID={setSearchID}
      />
    </main>
  );
}
