export default function Socials() {
  const social = ["tiktok", "twitter", "instagram"];

  return (
    <div class="social-icons">
      {social.map((item) => (
        <a href="#" target="_blank" rel="noreferrer" class="social-icon">
          <i class={`fa-brands fa-${item}`}></i>
        </a>
      ))}
    </div>
  );
}
