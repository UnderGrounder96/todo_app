import { Socials } from "./Socials.jsx";

export default function Header() {
  return (
    <header>
      <nav class="section-center">
        <a href="/">
          <i class="logo">checklist</i>
        </a>

        <Socials />
      </nav>
    </header>
  );
}
