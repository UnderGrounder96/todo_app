export const data = [
  {
    id: 1,
    title: "burn diary",
    desc: "lorem ipsum",
    date: "15-01-2024",
  },
  {
    id: 2,
    title: "clean the room",
    desc: "my random desc",
    date: "01-12-2023",
  },
  {
    id: 3,
    title: "finish preact project",
    desc: "foo bar",
    date: "22-10-2023",
  },
];
