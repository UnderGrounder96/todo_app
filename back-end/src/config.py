#!/usr/bin/env python3


from os import getenv
from datetime import timedelta

from dotenv import load_dotenv

load_dotenv()


# configs
server_error = "Could not process input"

days = int(getenv("SESSION_LIFETIME"))

SECRET_KEY = getenv("SECRET_KEY")
SESSION_COOKIE_NAME = getenv("SESSION_NAME")
PERMANENT_SESSION_LIFETIME = timedelta(days=days)


# mongoDB
MONGO_DB = "users"
MONGO_PORT = int(getenv("MONGO_PORT"))
MONGO_HOST = getenv("MONGO_HOST")
MONGO_USER = getenv("MONGO_USER")
MONGO_PASSWORD = getenv("MONGO_PASSWORD")
MONGO_URI_URL = f"{MONGO_USER}:{MONGO_PASSWORD}@{MONGO_HOST}:{MONGO_PORT}"
MONGO_URI = f"mongodb://{MONGO_URI_URL}/{MONGO_DB}?authSource=admin"
