#!/usr/bin/env python3

import config

from bson import ObjectId

from flask import Flask, request

from flask_cors import CORS
from flask_pymongo import PyMongo

app = Flask(__name__)
CORS(app)

mongo = PyMongo(app, uri=config.MONGO_URI)
todos = mongo.db.todos

app.config.from_pyfile("config.py")


@app.route("/", methods=["GET"])
def get_all_todos():
    todos_response = []
    for item in todos.find():
        item['id'] = str(item.pop('_id'))
        todos_response.append(item)

    return {"body": todos_response, "count": len(todos_response)}, 200


@app.route("/<string:post_id>", methods=["GET"])
def get_single_todo(post_id):
    element = todos.find_one({"_id": ObjectId(post_id)})

    todo_response = element.copy()
    todo_response['id'] = str(todo_response.pop('_id'))
    return {"body": todo_response, "count": 1}, 200


@app.route("/", methods=["POST"])
def post_todo():
    element = request.get_json(force=True)
    if "data" not in element:
        return {"error": config.server_error, "status": 400}, 400

    body = todos.insert_one(element['data']).inserted_id
    return {"body": str(body), "count": 1}, 200


@app.route("/<string:patch_id>", methods=["PATCH"])
def patch_todo(patch_id):
    element = request.get_json(force=True)
    if "data" not in element:
        return {"error": config.server_error, "status": 400}, 400

    body = todos.update_one({"_id": ObjectId(patch_id)}, {
                            "$set": element['data']}).modified_count
    return {"body": patch_id, "count": body}, 200


@app.route("/<string:delete_id>", methods=["DELETE"])
def delete_todo(delete_id):
    element = todos.delete_one({"_id": ObjectId(delete_id)})

    if element.deleted_count == 1:
        return {"body": f"deleted - {delete_id}", "count": 1}, 200

    return {"error": config.server_error, "status": 400}, 400
